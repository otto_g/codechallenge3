import React, { Component } from "react";
import "./App.css";
import AppHeader from "./Header";
import AppProduct from "./AppProduct";
import AppProduct2 from "./AppProduct2";

import { BrowserRouter, Route } from "react-router-dom";
import AddProduct from "./AddProduct";

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <div>
            <Route exact path="/" component={AppProduct} />
            <Route path="/add" component={AddProduct} />
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
