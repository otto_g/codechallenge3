import React, { Component } from "react";
import AppHeader from "./Header";

import {
  Container,
  Row,
  Col,
  Button,
  Form,
  FormGroup,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle
} from "reactstrap";
import axios from "axios";

class AddProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      categories: "",
      content: ""
    };
  }

  handleTitleChange = event => {
    this.setState({ title: event.target.value });
  };

  handleImageLinkChange = event => {
    this.setState({ categories: event.target.value });
  };

  handleContentChange = event => {
    this.setState({ content: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();
    axios
      .post(`http://reduxblog.herokuapp.com/api/posts?key=otto`, {
        title: this.state.title,
        categories: this.state.categories,
        content: this.state.content
      })
      .then(response => {
        console.log(response);
        console.log(response.data);
        alert("Article was added");
        this.props.history.push("/");
      });
  };

  render() {
    console.log(this.state.title);
    return (
      <div>
        <Container>
          <AppHeader />

          <Row>
            <Col>
              <Row>
                <Col>
                  <Card
                    style={{
                      marginTop: "20px",
                      boxShadow: "5px 5px 20px gray"
                    }}
                  >
                    <CardBody
                      style={{
                        marginLeft: "30px"
                      }}
                    >
                      <CardTitle className="card-deck">
                        <h2>Add Product Data</h2>
                      </CardTitle>
                      <CardText>
                        <Form onSubmit={this.handleSubmit}>
                          <FormGroup>
                            <h5>Title:</h5>
                            <p>
                              <input
                                style={{
                                  width: "100%"
                                }}
                                type="text"
                                name="this.state.title"
                                onChange={this.handleTitleChange}
                              />
                            </p>
                          </FormGroup>
                          <FormGroup>
                            <h5>Image Link:</h5>
                            <p>
                              <input
                                style={{
                                  width: "100%"
                                }}
                                type="text"
                                name="this.state.categories"
                                onChange={this.handleImageLinkChange}
                              />
                            </p>
                          </FormGroup>
                          <FormGroup>
                            <h5>Content:</h5>
                            <p>
                              <input
                                style={{
                                  width: "100%"
                                }}
                                type="text"
                                name="this.state.content"
                                onChange={this.handleContentChange}
                              />
                            </p>
                          </FormGroup>
                          <Button color="primary" type="submit">
                            Add Item
                          </Button>
                        </Form>
                      </CardText>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
        <div>
          <div />
        </div>
      </div>
    );
  }
}

export default AddProduct;
