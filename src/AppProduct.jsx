import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Jumbotron,
  Button
} from "reactstrap";

import axios from "axios";
import AppHeader from "./Header";

class AppProduct extends React.Component {
  constructor() {
    super();
    this.state = {
      products: []
    };
  }

  componentDidMount() {
    axios
      .get(`http://reduxblog.herokuapp.com/api/posts?key=otto`) //backtick -- above escape key
      .then(response => {
        this.setState({
          products: response.data
        });
      });
  }

  handleDelete = productID => {
    //const products_local = [...this.state.products ];
    const products_local = this.state.products.filter(p => p.id != productID);

    axios
      .delete(`http://reduxblog.herokuapp.com/api/posts/${productID}`)
      .then(response => {
        console.log("getting response after deleting");
        console.log(response);
        alert("removed");
        this.setState({ products: products_local });
        //this.props.history.push("/");
        //window.location.reload();
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    console.log(this.state.products);
    return (
      <div>
        <Container>
          <AppHeader />

          <Row className="d-flex">
            <Col>
              <Row>
                {this.state.products.map((product, index) => (
                  <Col key={index} md="4">
                    <Card
                      style={{
                        marginTop: "20px",
                        boxShadow: "5px 5px 20px gray"
                      }}
                    >
                      <CardImg
                        top
                        width="100%"
                        src={product.categories}
                        height="250px"
                      />
                      <CardBody
                        className="p-4"
                        style={{
                          marginLeft: "10px"
                        }}
                      >
                        <CardTitle className="card-deck">
                          <h4>{product.title}</h4>
                        </CardTitle>
                        <CardText className="card-deck">
                          {product.content}
                        </CardText>
                        <Button
                          className="card-deck"
                          onClick={e => this.handleDelete(product.id)}
                        >
                          Delete
                        </Button>
                      </CardBody>
                    </Card>
                  </Col>
                ))}
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default AppProduct;
